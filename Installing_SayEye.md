# Installing SayEye components

SayEye is in beta version, so you will need to intall some more components in order to make it work.
Please follow the instruction in the exact order:

1. Install the [Linux kernel update](https://docs.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package) for Windows 10. This is needed for the proper functioning of Docker
1. Install [Docker](https://www.docker.com/products/docker-desktop)
   - Download and run the installer
   - Run Docker and accept the agreement
   - Skip the tutorial
   - Docker is up and running, you can close it now (it will stay in background).
1. Install Toby eyetracker drivers
1. Install Toby Experience to calibrate the eyetracker https://www.microsoft.com/store/productId/9NK75KF67S2N
1. Connect and calibrate the eyetracker
1. Install [SayEye](https://gitlab.com/opendot/sayeye/sayeye-documentation/-/raw/main/SayeyeInstaller.exe)
1. Restart the system
1. Docker and SayEye should initialize throught windows powershell

